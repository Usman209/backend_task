import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CovidDetailModule } from './covid_detail/covid_detail.module';
import { MongooseModule } from "@nestjs/mongoose";


@Module({
  imports: [  MongooseModule.forRoot('mongodb://localhost/covid_database', {
      useNewUrlParser: true
    }),CovidDetailModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
