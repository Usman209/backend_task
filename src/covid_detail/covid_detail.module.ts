import { Module } from '@nestjs/common';
import { CovidDetailService } from './covid_detail.service';
import { CovidDetailController } from './covid_detail.controller';
import { CovidDetailSchema } from './schemas/covid_detail.shema';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
	imports: [ MongooseModule.forFeature([ { name: 'CovidDetail', schema: CovidDetailSchema } ]) ],

	controllers: [ CovidDetailController ],
	providers: [ CovidDetailService ]
})
export class CovidDetailModule {}
