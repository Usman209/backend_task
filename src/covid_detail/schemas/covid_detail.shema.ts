import { Schema } from 'mongoose';

export const CovidDetailSchema = new Schema({
	YearWeekISO: String,
	ReportingCountry: String,
	Denominator: String,
	NumberDosesReceived: String,
	NumberDosesExported: String,
	FirstDose: String,
	SecondDose: String,
	DoseAdditional1: String,
	DoseAdditional2: String,
	DoseAdditional3: String,
	UnknownDose: String,
	Region: String,
	TargetGroup: String,
	Vaccine: String,
	Population: String
});
