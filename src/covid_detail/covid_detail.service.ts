import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { CovidDetail } from './interfaces/covid_detail.interface';
import { CovidDetailDto } from './dto/covid_detail.dto';
import { CovidDetailFilterDto } from './dto/covid_detail_filter.dto';

@Injectable()
export class CovidDetailService {
	constructor(@InjectModel('CovidDetail') private readonly CovidDetailModel: Model<CovidDetail>) {}

	create(covidDetailDto: CovidDetailDto) {
		return 'This action adds a new covidDetail';
	}

	async findAll(): Promise<CovidDetail[]> {
		const data = await this.CovidDetailModel.find();

		return data;
	}

	async getFilteredProducts(filterDto: CovidDetailFilterDto): Promise<CovidDetail[]> {
		const { c, dateFrom, dateTo, range } = filterDto;

		var data = await this.CovidDetailModel.find({});

		const dataModel = await this.CovidDetailModel.aggregate([
			{
				$match: {
					ReportingCountry: c,
					YearWeekISO: {
						$gte: dateFrom,
						$lte: dateTo
					}
				}
			},

			{
				$group: {
					_id: { Start: '$YearWeekISO', End: dateFrom },
					NumberDosesReceived: {
						$sum: 1
					}
				}
			}
		]);

		return dataModel;
	}
}
