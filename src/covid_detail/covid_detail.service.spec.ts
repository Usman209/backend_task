import { Test, TestingModule } from '@nestjs/testing';
import { CovidDetailService } from './covid_detail.service';

describe('CovidDetailService', () => {
  let service: CovidDetailService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CovidDetailService],
    }).compile();

    service = module.get<CovidDetailService>(CovidDetailService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
