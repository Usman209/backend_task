import {
	Controller,
	Post,
	Res,
	HttpStatus,
	Body,
	Get,
	Param,
	NotFoundException,
	Delete,
	Query,
	Put
} from '@nestjs/common';
import { CovidDetailService } from './covid_detail.service';
import { CovidDetailFilterDto } from './dto/covid_detail_filter.dto';

@Controller('covid-detail')
export class CovidDetailController {
	constructor(private readonly covidDetailService: CovidDetailService) {}

	@Get('/')
async	findAll(@Res() res, @Query() filterDto: CovidDetailFilterDto) {
console.log('here 120');

       if (Object.keys(filterDto).length) {
            var products = await this.covidDetailService.getFilteredProducts(filterDto);
                    return res.status(HttpStatus.OK).json(products);


        }
        
         products = await this.covidDetailService.findAll();
        return res.status(HttpStatus.OK).json(products);
  
  }

}
