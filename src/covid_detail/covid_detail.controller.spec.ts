import { Test, TestingModule } from '@nestjs/testing';
import { CovidDetailController } from './covid_detail.controller';
import { CovidDetailService } from './covid_detail.service';

describe('CovidDetailController', () => {
  let controller: CovidDetailController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CovidDetailController],
      providers: [CovidDetailService],
    }).compile();

    controller = module.get<CovidDetailController>(CovidDetailController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
