export class CovidDetailDto {
	readonly YearWeekISO: string;
	readonly ReportingCountry: string;
	readonly Denominator: string;
	readonly NumberDosesReceived: string;
	readonly NumberDosesExported: string;
	readonly FirstDose: string;
	readonly SecondDose: string;
	readonly DoseAdditional1: string;
	readonly DoseAdditional2: string;
	readonly DoseAdditional3: string;
	readonly UnknownDose: string;
	readonly Region: string;
	readonly TargetGroup: string;
	readonly Vaccine: string;
	readonly Population: string;
}
