export class CovidDetailFilterDto {
	c: string;
	dateFrom: string;
	dateTo: string;
	range: number;
}
